$(document).ready(function() {
  $("#image-carousel").slick({
    lazyLoad: 'ondemand',
    dots: true,
    arrows: true
  });

  $('#show-qr-btn').click(function() {
    retrieveQrCode();
  });

});

function performCheckin() {

};

function retrieveQrCode() {
  $.ajax({
    type: 'POST',
    url: '/qrcodes.json',
    data: { "qr_code": { "text": "qwerty" } },
    success: function(data) {
      alert(data);
    }
  });

};
