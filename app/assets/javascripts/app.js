/**
 * Created by kerpgasm on 9/7/15.
 */

var geoPosition = {};

function getLocation() {
  if(navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(loadPosition, positionError);
  } else {
    alert("Without Geolocation, you can only search for RXN Locations by typing in a location.");
  }
}

function positionError(error) {
  switch(error.code) {
    case error.PERMISSION_DENIED:
      break;
    case error.POSITION_UNAVAILABLE:
      break;
    case error.TIMEOUT:
      break;
    case error.UNKNOWN_ERROR:
      break;
  }
}

function loadPosition(position) {
  geoPosition.lat = position.coords.latitude;
  geoPosition.lon = position.coords.longitude;

  if(geoPosition.lat != null && geoPosition.lon != null) {
    $("#lat").val(geoPosition.lat);
    $("#lon").val(geoPosition.lon);
  }
}

window.onload = function() {
  getLocation();
};
