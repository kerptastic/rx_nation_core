/**
 * Created by kerpgasm on 9/7/15.
 */

$(document).ready(function($) {
  $(".clickable-row").click(function() {
    $(this).addClass("highlighted-row");
    window.document.location = $(this).data("href");
    $(this).removeClass("highlighted-row")
  });
});