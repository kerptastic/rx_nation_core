require 'securerandom'
require 'bcrypt'

#
# Represents an AuthToken implementation.
#
class AuthToken
  include Mongoid::Document
  field :secret_id, type: String
  field :hashed_secret, type: String
  field :client_id, type: String
  field :app_type, type: String
  field :expiration, type: DateTime

  validates :secret_id, presence: true, uniqueness: true
  validates :hashed_secret, presence: true
  validates :app_type, presence: true
  validates :client_id, presence: true

  after_initialize :default_values
  after_initialize :generate_secret_id
  after_initialize :generate_secret

  attr_accessor :secret

  belongs_to :user

  #
  #
  #
  def self.find_authenticated(credentials)
    token = AuthToken.where(secret_id: credentials[:secret_id],
                            client_id: credentials[:client_id]).first

    token if token && token.secret?(credentials[:secret])
  end

  #
  #
  #
  def secret?(secret)
    password_from_hash == secret
  end

  private

  #
  #
  #
  def password_from_hash
    BCrypt::Password.new(hashed_secret)
  end

  #
  #
  #
  def default_values
    self.expiration = DateTime.current + 1.month
  end

  #
  #
  #
  def generate_secret_id
    loop do
      self.secret_id = SecureRandom.hex(8)

      #puts AuthToken.where(secret_id: 'foobar').inspect

      break unless AuthToken.where(secret_id: secret_id).count > 0
    end
  end

  #
  #
  #
  def generate_secret
    self.secret = SecureRandom.urlsafe_base64(32)
    self.hashed_secret = BCrypt::Password.create(secret, cost: cost)
  end

  #
  #
  #
  def cost
    Rails.env.test? ? 1 : 10
  end
end
