#
# Represents a Checkin object.
#
class Checkin
  include Mongoid::Document

  field :when, type: DateTime

  belongs_to :user, autosave: true
  belongs_to :affiliate, autosave: true

  validates :when, presence: true
  validates :user, presence: true
  validates :affiliate, presence: true
end
