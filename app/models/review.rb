#
# Represents a Review object.
#
class Review
  include Mongoid::Document
  field :comment, type: String
  field :num_stars, type: Integer
  field :characteristics, type: Array

  validates :num_stars, presence: true, inclusion: 0..5
  validates :comment, length: { maximum: 500 }
  belongs_to :affiliate
end
