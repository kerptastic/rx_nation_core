#
# Represents a Billing Info object.
#
class BillingInfo
  include Mongoid::Document

  field :card_company, type: String
  field :card_type, type: String
  field :card_number, type: String
  field :card_exp_month, type: String
  field :card_exp_year, type: Integer

  validates :card_company, presence: true, inclusion: {
    in: %w(Visa Mastercard American\ Express Discover) }
  validates :card_type, presence: true, inclusion: { in: %w(Debit Credit) }
  validates :card_number, presence: true, credit_card: { valid: true }
  validates :card_exp_month, presence: true
  validates :card_exp_year, presence: true

  belongs_to :subscription
end
