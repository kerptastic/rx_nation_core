require 'set'

include Geocoder::Model::Mongoid

#
#
#
class Affiliate
  include Mongoid::Document
  field :name, type: String
  field :address1, type: String
  field :address2, type: String
  field :city, type: String
  field :state, type: String
  field :zipcode, type: Integer
  field :coordinates, type: Array
  field :default_img_path, type: String
  field :image_paths, type: Array
  field :is_featured, type: Boolean
  field :url_value, type: String

  attr_accessor :distance_from_search
  attr_accessor :avg_rating

  validates :name, presence: true, length: { in: 2..40 }
  after_validation :make_url_value

  validates :address1, presence: true
  validates :city, presence: true
  validates :state, presence: true
  validates :zipcode,
            presence: true,
            format: { with: /\A\d{5}-\d{4}|\A\d{5}\z/,
                      message: 'should be 12345 or 12345-1234' }

  has_many :subscriptions
  has_many :checkins
  has_many :reviews
  has_many :users

  geocoded_by :address

  after_validation :geocode, if: (lambda do |obj|
    obj.coordinates.nil? ||
      (obj.coordinates.present? && obj.coordinates_changed?)
  end)

  # accepts_nested_attributes_for :subscription

  #
  # Static method - used to find all affiliates near the given coordinates
  #
  def self.search(coordinates)
    Affiliate.geo_near(coordinates)
  end

  #
  # Used to retrieve the geocoding compliant address string
  #
  def address
    a = "#{address1}"
    a = "#{a} #{address2}" if address2?
    a = "#{a}, #{city}, #{state} #{zipcode}"
    a
  end

  #
  # Retrieve the average review rating for this affiliate
  #
  def avg_review
    # note - inject applies a binary operation on all elements and sets the
    # initial value to what you pass into inject
    reviews.count == 0 ? 0 : Float(reviews.inject(0) do |total, r|
      total + r.num_stars
    end) / Float(reviews.count)
  end

  #
  # Returns a list of characteristics as a comma-separated string
  #
  def characteristics
    chars = Set.new

    reviews.each do |review|
      chars.merge(review.characteristics)
    end

    chars.to_a.join(', ')
  end

  #
  # Sets the distance in miles from this affiliate to the coordinates passed
  # in.  This is a helper method essentially to be able to print out on the
  # search results and affiliate page the distance from where the user is
  # located, or searched near.
  #
  def set_distance_from_search(coordinates)
    self.distance_from_search = distance_to(coordinates).round(2)
  end

  protected

  #
  # Creates the url name to replace the id on the show route
  #
  def make_url_value
    self.url_value = name.gsub(/\s+/, '') unless name.nil?
  end
end
