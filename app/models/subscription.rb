#
# Represents a Subscription object.
#
class Subscription
  include Mongoid::Document
  field :plan, type: String
  field :cost, type: Float
  field :joined_date, type: Date

  validates :plan,
            presence: true,
            inclusion: {
              in: %w(Monthly\ Member Yearly\ Member),
              message: '%{value}: is not a valid plan.' }
  validates :cost,
            presence: true,
            numericality: true,
            format: { with: /\A\d{1,5}(\.\d{0,2})?\z/ }
  validates :joined_date, presence: true
  validates :billing_info, presence: true
  validates :affiliate, presence: true

  belongs_to :affiliate
  has_one :billing_info

  accepts_nested_attributes_for :billing_info
end
