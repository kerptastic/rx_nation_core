#
# Represents a User object.
#
class User
  include Mongoid::Document
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  ## Database authenticatable
  field :email,              type: String, default: ''
  field :encrypted_password, type: String, default: ''

  ## Recoverable
  field :reset_password_token,   type: String
  field :reset_password_sent_at, type: Time

  ## Rememberable
  field :remember_created_at, type: Time

  ## Trackable
  field :sign_in_count,      type: Integer, default: 0
  field :current_sign_in_at, type: Time
  field :last_sign_in_at,    type: Time
  field :current_sign_in_ip, type: String
  field :last_sign_in_ip,    type: String

  ## Confirmable
  # field :confirmation_token,   type: String
  # field :confirmed_at,         type: Time
  # field :confirmation_sent_at, type: Time
  # field :unconfirmed_email,    type: String # Only if using reconfirmable

  ## Lockable
  # Only if lock strategy is :failed_attempts
  # field :failed_attempts, type: Integer, default: 0

  # Only if unlock strategy is :email or :both
  # field :unlock_token,    type: String
  # field :locked_at,       type: Time

  field :first_name, type: String
  field :last_name, type: String
  field :address1, type: String
  field :address2, type: String
  field :city, type: String
  field :state, type: String
  field :zipcode, type: String
  field :is_admin, type: Boolean, default: false

  validates :first_name, presence: true, length: { in: 1..30 }
  validates :last_name, presence: true, length: { in: 1..40 }
  validates :address1, presence: true, length: { in: 1..50 }
  validates :city, presence: true, length: { in: 1..50 }
  validates :state, presence: true, length: { is: 2 }
  validates :zipcode, presence: true, length: { is: 5 }
  validates :affiliate, presence: true

  belongs_to :affiliate
  has_many :checkins
  has_many :auth_tokens
end
