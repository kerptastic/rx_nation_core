json.array!(@affiliates) do |affiliate|
  json.extract! affiliate, :id, :name, :address1, :address2, :city, :state,
                :zipcode, :coordinates, :default_img_path, :image_paths,
                :is_featured, :url_value, :checkins, :reviews, :make_url_value,
                :get_avg_review, :get_characteristics, :full_address_geocode,
                :full_address_print
  json.url affiliate_url(affiliate, format: :json)
end
