#
#
#
class CreditCardValidator < ActiveModel::EachValidator
  COMPANY_REGEX = {
    'VISA' => /^4[0-9]{12}(?:[0-9]{3})$/,
    'MASTERCARD' => /^5[1-5][0-9]{14}$/,
    'DISCOVER' => %r{ ^65[4-9][0-9]{13}|64[4-9][0-9]{13}|6011[0-9]{12}|(622(
                    ?:12[6-9]|1[3-9][0-9]|[2-8][0-9][0-9]|9[01][0-9]|92[0-5])
                    [0-9]{10})$ },
    'AMERICAN EXPRESS' => /^3[47][0-9]{13}$/
  }

  #
  #
  #
  def validate_each(record, attribute, value)
    return unless options.key?(:valid)
    return if record.card_company.nil?

    card_regex = COMPANY_REGEX[record.card_company.upcase]

    return unless (value =~ card_regex).nil?

    record.errors[attribute] <<
      "is not a valid #{record.card_company} card number."
  end
end
