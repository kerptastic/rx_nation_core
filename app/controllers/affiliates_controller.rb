#
# Controller managing Affiliates display, search, and listing.
#
class AffiliatesController < ApplicationController
  before_action :require_authenticated, only: [:new, :create, :edit, :update,
                                               :destroy, :create_checkin,
                                               :index_checkins]
  before_action :require_admin, only: [:new, :create, :edit, :update, :destroy,
                                       :index_checkins]
  before_action :set_affiliate, only: [:show, :edit, :update, :destroy,
                                       :new_checkin, :index_checkins]

  #
  # GET /affiliates
  # GET /affiliates.json
  #
  def index
    lat = params['lat']
    lon = params['lon']

    search_location = params['search_location']

    if !search_location.nil? && !search_location.blank?
      # TODO: perform a search based on address and not lat long
    elsif valid_lat_lon(lat, lon)
      @affiliates = affiliates_with_distance_from_search(lat, lon)
      # else
      # TODO: we have a problem here...
    end

    @back_button_text = @affiliates.nil? ? 'Back' : 'Home'
    @page_text = 'Find RXN Gym'

    # respond_to do |format|
    #   format.html { redirect_to '/affiliates/search',
    #                 notice: 'Search functioned.' }
    #   format.json { render :show, status: :ok, location: @affiliate }
    # end
  end

  def valid_lat_lon(lat, lon)
    !lat.nil? && !lon.nil?
  end

  #
  #
  #
  def affiliates_with_distance_from_search(lat, lon)
    # IMPORTANT
    # for some reason the API was built to take lon/lat vs lat/lon
    Affiliate.includes(:reviews).search([Float(lon), Float(lat)]).map do |a|
      a.set_distance_from_search([lat, lon])
      a
    end
  end

  #
  # GET /affiliates/<affiliate_url_value>
  # GET /affiliates/<affiliate_url_value>.json
  #
  def show
    @page_text = @affiliate.name
  end

  #
  # GET /affiliates/new
  #
  def new
    @affiliate = Affiliate.new
    # @affiliate.build_subscription
  end

  #
  # GET /affiliates/1/edit
  #
  def edit
  end

  #
  # POST /affiliates
  # POST /affiliates.json
  #
  def create
    @affiliate = Affiliate.new(affiliate_params)

    respond_to do |format|
      if @affiliate.save
        format.html { redirect_to action: :show, id: @affiliate.url_value }
        format.json { render :show, status: :created, location: @affiliate }
      else
        format.html { render :new }
        format.json do
          render json: @affiliate.errors, status: :unprocessable_entity
        end
      end
    end
  end

  #
  # PATCH/PUT /affiliates/<affiliate_url_value>
  # PATCH/PUT /affiliates/<affiliate_url_value>.json
  #
  def update
    respond_to do |format|
      if @affiliate.update(affiliate_params)
        format.html { redirect_to action: :show, id: @affiliate.url_value }
        format.json { render :show, status: :ok, location: @affiliate }
      else
        format.html { render :edit }
        format.json do
          render json: @affiliate.errors, status: :unprocessable_entity
        end
      end
    end
  end

  #
  # DELETE /affiliates/<affiliate_url_value>
  # DELETE /affiliates/<affiliate_url_value>.json
  #
  def destroy
    @affiliate.destroy unless @affiliate.nil?

    respond_to do |format|
      format.html { redirect_to affiliates_url }
      format.json { head :no_content }
    end
  end

  #
  #
  #
  def new_checkin
    @checkin = Checkin.new
    @affiliates = Affiliate.all
    #     @checkin.when = DateTime.now
    #     @checkin.user = current_user
  end

  #
  #
  #
  def create_checkin
    affiliate = Affiliate.where(url_value: params[:id]).first

    @checkin = Checkin.new(affiliate: affiliate, when: DateTime.now,
                           user: current_user)

    # @checkin.affiliate = affiliate
    # @checkin.when = DateTime.now
    # @checkin.user = current_user

    respond_to do |format|
      if @checkin.save
        format.html { redirect_to action: :show, id: affiliate.url_value }
        # format.json { render :show, status: :created, location: @checkin }
      else
        format.html { render :show, location: affiliate }
        # format.json { render json: @checkin.errors,
        #              status: :unprocessable_entity }
      end
    end
  end

  #
  #
  #
  def index_checkins
    @checkins = Checkin.where(affiliate: @affiliate)
  end

  private

  #
  # Sets the affiliate based on the id parameter that was passed via the URL
  #
  def set_affiliate
    value = params[:id]
    @affiliate = Affiliate.where(url_value: value).first
  end

  #
  # Never trust parameters from the scary internet, only allow the white list
  # through.
  #
  def affiliate_params
    params.require(:affiliate).permit(
      :name, :address1, :address2, :city, :state, :zipcode, :default_img_path,
      :is_featured, subscription_attributes: [
        :plan, :cost, :joined_date, billing_info_attributes: [
          :card_company, :card_type, :card_number, :card_exp_month,
          :card_exp_year]])
  end
end
