#
# Top level Users module for all controllers pertaining to users.
#
module Users
  #
  # Controller for Devise Sessions. Used to create a session when a user logs
  # in.
  #
  class SessionsController < Devise::SessionsController
    #
    # POST /resource/sign_in
    #
    def create
      super

      return if invalid_headers?

      resolve_token(request.headers['HTTP_CLIENT_ID'],
                    request.headers['HTTP_APP_TYPE'])
    end

    private

    #
    #
    #
    def invalid_headers?
      request.headers['HTTP_CLIENT_ID'].nil? &&
        request.headers['HTTP_APP_TYPE'].nil?
    end

    #
    #
    #
    def resolve_token(client_id, app_type)
      token = current_user_token(client_id, app_type)

      token ||= initialize_token(client_id, app_type, current_user)

      token.expiration = generate_expiration

      token.save!
    end

    #
    # Retrieves the first auth token found for the current user, returns
    # nil if nothing exists.
    #
    def current_user_token(client_id, app_type)
      current_user.auth_tokens.where(client_id: client_id,
                                     app_type: app_type).first
    end

    #
    # Creates an expiration to use for a token.
    #
    def generate_expiration
      DateTime.current + 1.month
    end

    #
    # Initializes a new authorization token.
    #
    def initialize_token(client_id, app_type, user)
      AuthToken.new(client_id: client_id, app_type: app_type, user: user)
    end
  end
end
