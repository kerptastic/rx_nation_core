# class CheckinsController < ApplicationController
#   before_action :require_authenticated, only: [:new, :create, :edit, :update,
#                                                :destroy]
#   before_action :require_admin, only: [:edit, :update, :destroy]
#   before_action :set_checkin, only: [:show, :edit, :update, :destroy]
#   before_action :init_checkin_data, only: [:new]
#
#   def new
#     init_checkin_data
#   end
#
#   def create
#     @checkin = Checkin.new(checkin_params)
#     @checkin.when = DateTime.now
#     @checkin.user = current_user
#
#     respond_to do |format|
#       if @checkin.save
#         format.html do
#           redirect_to @checkin,
#           notice: 'Checkin was successfully created.'
#         end
#         format.json { render :show, status: :created, location: @checkin }
#       else
#         format.html { render :new }
#         format.json do
#           render json: @checkin.errors,
#           status: :unprocessable_entity
#         end
#       end
#     end
#   end
#
#   def destroy
#     @checkin.destroy if not @checkin.nil?
#
#     respond_to do |format|
#       format.html { render :new }
#     end
#   end
#
#   private
#   def set_checkin
#     @checkin = Checkin.find(params[:id])
#   end
#
#   # Never trust parameters from the scary internet, only allow the white list
#   def checkin_params
#     params.require(:checkin).permit(:affiliate, :user)
#   end
#
#   #
#   # Initializes the checkin and affiliate list
#   #
#   def init_checkin_data
#     @checkin = Checkin.new
#     @affiliates = Affiliate.all
#   end
# end
