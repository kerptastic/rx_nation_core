#
# Base controller that all controllers will inherit from, containing high level
# functions of common functionality.
#
class ApplicationController < ActionController::Base
  before_filter :authenticate_token
  # before_filter :debug

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  protected

  #
  #
  # rubocop:disable Metrics/AbcSize
  # :nocov:
  def debug
    client_id = request.headers['HTTP_CLIENT_ID']
    app_type = request.headers['HTTP_APP_TYPE']
    secret = request.headers['HTTP_SECRET']
    secret_id = request.headers['HTTP_SECRET_ID']

    puts "CLIENT ID: #{client_id}"
    puts "APP TYPE : #{app_type}"
    puts "SECRET   : #{secret}"
    puts "SECRET ID: #{secret_id}"
  end
  # :nocov:

  #
  #
  #
  def authenticate_token
    # if current_user.nil? and request.headers['HTTP_APP_TYPE'].eql? 'native'
    #   @current_token = find_users_token
    #
    #   if @current_token.try(:user)
    #     puts "### SIGNING IN #{@current_token.user}"
    #     sign_in @current_token.user, :store => false
    #   end
    # end
  end

  #
  #
  #
  def find_users_token
    # credentials = {}
    #
    # credentials[:secret] = request.headers['HTTP_SECRET']
    # credentials[:secret_id] = request.headers['HTTP_SECRET_ID']
    # credentials[:client_id] = request.headers['HTTP_CLIENT_ID']
    #
    # AuthToken.find_authenticated(credentials)
  end

  #
  #
  #
  def require_admin
    return unless current_user.nil? || !current_user.is_admin?

    flash[:error] = 'You must be an administrator to perform this function.'
    render file: 'public/404.html', status: :unauthorized
  end

  #
  #
  #
  def require_authenticated
    return unless current_user.nil?
    return unless @current_token.nil? ||
                  @current_token.expiration > DateTime.current

    flash[:error] = 'You must be logged in to perform this function.'
    redirect_to new_user_session_path
  end
end
