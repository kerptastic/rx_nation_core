# This file should contain all the record creation needed to seed the database
# with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db
#  with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

case Rails.env
when 'development'
  # ============= Affiliates =================
  cgv = Affiliate.create(
    name: 'Crossfit Greenwood Village',
    address1: '5500 DTC Pkwy',
    address2: '#1118',
    city: 'Greenwood Village',
    state: 'CO',
    zipcode: '80111',
    default_img_path: '/assets/crossfit_greenwood_village/default.jpg',
    image_paths: ['/assets/crossfit_greenwood_village/default.jpg',
                  '/assets/crossfit_greenwood_village/1.jpg',
                  '/assets/crossfit_greenwood_village/2.jpg'],
    is_featured: true
  )

  review = Review.create(
    comment: 'This is the comment for a review.',
    num_stars: '4',
    characteristics: %w(Barbell Bodyweight)
  )

  review.affiliate = Affiliate.where(name: 'Crossfit Greenwood Village').first
  review.save!

  review = Review.create(
    comment: 'This is another comment for a review.',
    num_stars: '3',
    characteristics: ['Bodyweight']
  )

  review.affiliate = Affiliate.where(name: 'Crossfit Greenwood Village').first
  review.save!

  sleep 1
  Affiliate.create(
    name: 'XS Crossfit',
    address1: '6522 Ponderosa Dr.',
    address2: '',
    city: 'Parker',
    state: 'CO',
    zipcode: '80138',
    default_img_path: '/assets/xs_crossfit/default.jpg',
    is_featured: false
  )

  sleep 1
  Affiliate.create(
    name: 'Highlands Ranch Crossfit',
    address1: '9337 Commerce Center St.',
    address2: '#C5',
    city: 'Highlands Ranch',
    state: 'CO',
    zipcode: '80129',
    default_img_path: '/assets/highlands_ranch_crossfit/default.jpg',
    is_featured: false
  )

  sleep 1
  Affiliate.create(
    name: 'Front Range Crossfit',
    address1: '1338 South Valentia St.',
    address2: '#182',
    city: 'Denver',
    state: 'CO',
    zipcode: '80247',
    default_img_path: '/assets/front_range_crossfit/default.jpg',
    is_featured: false
  )

  sleep 1
  Affiliate.create(
    name: 'Crossfit Ascend',
    address1: '6622 Martin St.',
    address2: '',
    city: 'Rome',
    state: 'NY',
    zipcode: '13440',
    default_img_path: '/assets/crossfit_ascend/default.png',
    is_featured: false
  )

  sleep 1
  Affiliate.create(
    name: 'Crossfit Mohawk Valley',
    address1: '9647 River Rd.',
    address2: '',
    city: 'Marcy',
    state: 'NY',
    zipcode: '13403',
    default_img_path: '/assets/crossfit_mohawk_valley/default.png',
    is_featured: false
  )

  sleep 1
  Affiliate.create(
    name: 'Crossfit Lodo',
    address1: '2363 Blake St.',
    address2: '',
    city: 'Denver',
    state: 'CO',
    zipcode: '80205',
    default_img_path: '/assets/crossfit_lodo/default.png',
    is_featured: false
  )

  #============= Users ====================
  User.create(
    email: 'josh.kierpiec@gmail.com',
    first_name: 'Josh',
    last_name: 'Kierpiec',
    address1: '8901 E Nichols Pl',
    address2: '',
    city: 'Centennial',
    state: 'CO',
    zipcode: '80112',
    password: 'password',
    affiliate: cgv,
    is_admin: true
  )

  User.create(
    email: 'nate.kierpiec@gmail.com',
    password: 'password',
    is_admin: true
  )

  User.create(
    email: 'bperry422@gmail.com',
    password: 'password',
    is_admin: false
  )
when 'test'

end
