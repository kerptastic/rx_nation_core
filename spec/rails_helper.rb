# This file is copied to spec/ when you run 'rails generate rspec:install'
require File.expand_path('../../config/environment', __FILE__)

# Prevent database truncation if the environment is production
error_msg = 'The Rails environment is running in production mode!'
abort(error_msg) if Rails.env.production?

require 'rspec/rails'
require 'spec_helper'
require 'support/controller_macros'

require 'devise'
require 'capybara/rails'

# Setup database for testing
require 'rake'

RxNationCore::Application.load_tasks

Rake::Task['db:purge'].invoke
Rake::Task['db:setup'].invoke

Geocoder.configure(lookup: :test)

Geocoder::Lookup::Test.set_default_stub(
  [
    {
      'latitude' => 39.725449,
      'longitude' => -105.012704,
      'address' => '5912 E Colfax Ave, Denver, CO, USA',
      'state' => 'Colorado',
      'state_code' => 'CO',
      'country' => 'United States',
      'country_code' => 'US'
    }
  ]
)

# Add additional requires below this line. Rails is not loaded until this point!

# Requires supporting ruby files with custom matchers and macros, etc, in
# spec/support/ and its subdirectories. Files matching `spec/**/*_spec.rb` are
# run as spec files by default. This means that files in spec/support that end
# in _spec.rb will both be required and run as specs, causing the specs to be
# run twice. It is recommended that you do not name files matching this glob to
# end with _spec.rb. You can configure this pattern with the --pattern
# option on the command line or in ~/.rspec, .rspec or `.rspec-local`.
#
# The following line is provided for convenience purposes. It has the downside
# of increasing the boot-up time by auto-requiring all files in the support
# directory. Alternatively, in the individual `*_spec.rb` files, manually
# require only the support files necessary.
#
# Dir[Rails.root.join('spec/support/**/*.rb')].each { |f| require f }

RSpec.configure do |config|
  # RSpec Rails can automatically mix in different behaviours to your tests
  # based on their file location, for example enabling you to call `get` and
  # `post` in specs under `spec/controllers`.
  #
  # You can disable this behaviour by removing the line below, and instead
  # explicitly tag your specs with their type, e.g.:
  #
  #     RSpec.describe UsersController, :type => :controller do
  #       # ...
  #     end
  #
  # The different available types are documented in the features, such as in
  # https://relishapp.com/rspec/rspec-rails/docs
  config.infer_spec_type_from_file_location!
  config.use_transactional_fixtures = false

  config.include FactoryGirl::Syntax::Methods
  config.include Devise::TestHelpers, type: :controller
  config.include Warden::Test::Helpers
  # config.include Mongoid::Matchers, type: :model
  config.extend ControllerMacros, type: :controller
  Warden.test_mode!

  config.after do
    Warden.test_reset!
  end

  # config.before :each do
  #   Mongoid.default_session.collections.select {
  #     |c| c.drop unless /^system/.match(c.name)
  #   }
  # end
end
