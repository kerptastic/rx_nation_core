require 'rspec'
require 'rails_helper'

describe Users::SessionsController, type: :controller do
  before(:each) do
    @request.env['devise.mapping'] = Devise.mappings[:user]
    @request.headers['HTTP_CLIENT_ID'] = 'TestClientID'
    @request.headers['HTTP_APP_TYPE'] = 'TestAppType'
  end

  #
  # CREATE TESTS
  #
  describe 'POST #create' do
    context 'when a user successfully logs in' do
      it "creates a new token if one doesn't already exist" do
        # setup
        ctrl = Users::SessionsController.new
        user = create(:user)

        allow(request.env['warden']).to receive(:authenticate!).and_return(user)
        allow(ctrl).to receive(:current_user).and_return(user)

        # exercise
        post :create

        # verify
        expect(AuthToken.count).to eq(1)
        expect(user.auth_tokens.first.app_type).to eq('TestAppType')
        expect(user.auth_tokens.first.client_id).to eq('TestClientID')

        # teardown
        user.auth_tokens.each(&:destroy)

        user.destroy
      end

      it 'retrieves the current token if one already exists' do
        # setup
        user = create(:user_with_tokens)

        # exercise
        post :create

        # verify
        # expect(AuthToken.count).to eq(2)
        expect(user.auth_tokens.first.app_type).to eq('FactoryAppType')
        expect(user.auth_tokens.first.client_id).to eq('FactoryClientID')

        # teardown
        user.auth_tokens.each(&:destroy)
        user.destroy
      end
    end
  end

  #
  # HELPER METHOD TESTS
  #
  describe '#current_user_token' do
    context 'when client_id and app_type match a users token' do
      it 'retrieves a user token' do
        # setup
        ctrl = Users::SessionsController.new

        user = create(:user_with_tokens)

        allow(request.env['warden']).to receive(:authenticate!).and_return(user)
        allow(ctrl).to receive(:current_user).and_return(user)

        # exercise
        retrieved_token = ctrl.send(:current_user_token, 'FactoryClientID',
                                    'FactoryAppType')

        # verify
        expect(retrieved_token).to_not be_nil
        expect(retrieved_token).to be_valid

        # teardown
        user.auth_tokens.each(&:destroy)
        user.destroy
      end
    end

    context 'when client_id doesnt match but app_type does' do
      it 'retrieves nil' do
        # setup
        ctrl = Users::SessionsController.new

        user = create(:user_with_tokens)

        allow(request.env['warden']).to receive(:authenticate!).and_return(user)
        allow(ctrl).to receive(:current_user).and_return(user)

        # exercise
        retrieved_token = ctrl.send(:current_user_token, 'FactoryClientID',
                                    'foobar')

        # verify
        expect(retrieved_token).to be_nil

        # teardown
        user.auth_tokens.each(&:destroy)
        user.destroy
      end
    end

    context 'when app_type doesnt match but client_id does' do
      it 'retrieves nil' do
        # setup
        ctrl = Users::SessionsController.new

        user = create(:user_with_tokens)

        allow(request.env['warden']).to receive(:authenticate!).and_return(user)
        allow(ctrl).to receive(:current_user).and_return(user)

        # exercise
        retrieved_token = ctrl.send(:current_user_token,
                                    'foobar', 'FactoryAppType')

        # verify
        expect(retrieved_token).to be_nil

        # teardown
        user.auth_tokens.each(&:destroy)
        user.destroy
      end
    end

    context 'when app_type and client_id do not match' do
      it 'retrieves nil' do
        # setup
        ctrl = Users::SessionsController.new

        user = create(:user_with_tokens)

        allow(request.env['warden']).to receive(:authenticate!).and_return(user)
        allow(ctrl).to receive(:current_user).and_return(user)

        # exercise
        retrieved_token = ctrl.send(:current_user_token, 'foobar', 'foobar')

        # verify
        expect(retrieved_token).to be_nil

        # teardown
        user.auth_tokens.each(&:destroy)
        user.destroy
      end
    end
  end

  describe '#expiration_date' do
    context 'when expiration date is created' do
      it 'creates an expiration date 1 month from the current time' do
        # setup
        ctrl = Users::SessionsController.new
        allow(DateTime).to receive(:current).and_return DateTime.new(2015, 1, 1)

        # exercise
        exp = ctrl.send(:generate_expiration)

        # verify
        expect(exp).to eq DateTime.new(2015, 2, 1)

        # teardown
        # N/A
      end
    end
  end

  describe '#initialize_token' do
    context 'when a token is initialized' do
      it 'returns a valid token' do
        # setup
        ctrl = Users::SessionsController.new
        user = create(:user)

        # exercise
        token = ctrl.send(:initialize_token, 'foo', 'foo', user)

        # verify
        expect(token).to_not be_nil
        expect(token.user).to eq user

        # teardown
        user.destroy
        token.destroy
      end
    end
  end
end
