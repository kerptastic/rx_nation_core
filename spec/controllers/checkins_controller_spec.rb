require 'rails_helper'

# describe CheckinsController, type: :controller do
#   #
#   # NEW TESTS
#   #
#   describe 'GET #new' do
#     context 'without a user logged in' do
#       it 'should be redirected to sign in' do
#         get :new
#         expect(response).to redirect_to(new_user_session_path)
#       end
#     end
#
#     context 'with any user logged in:' do
#       sign_in_user
#
#       it 'should display a new checkin form' do
#         get :new
#         expect(response).to be_success
#         expect(response).to render_template(:new)
#       end
#     end
#   end
#
#   #
#   # CREATE TESTS
#   #
#   describe 'POST #create' do
#     context 'without a user logged in:' do
#       it 'should be redirected to sign in' do
#         get :create
#         expect(response).to redirect_to(new_user_session_path)
#       end
#     end
#
#     context 'with a regular user logged in with invalid parameters' do
#       user = sign_in_user
#
#       it 'should not accept parameter and re-render #new' do
#         post :create, checkin: { when: DateTime.new, user: user,
#                                  affiliate: nil }
#
#         expect(Checkin.count).to eq(0)
#         expect(response).to render_template(:new)
#       end
#     end
#
#     context 'with an admin user logged in with valid parameters' do
#       user = sign_in_user
#
#       it 'should redirect to created checkin page' do
#         checkin = build(:checkin)
#         affiliate = build(:affiliate)
#
#         post :create, checkin: { when: checkin.when, user: checkin.user,
#                                  affiliate: checkin.affiliate }
#
#         expect(Checkin.count).to eq(1)
#
#         created = Checkin.all.first
#
#         expect(response).to redirect_to("/checkins/#{created.id}")
#
#         # teardown
#         checkin.user.destroy
#         checkin.destroy
#       end
#     end
#   end
#
#   #
#   # DELETE TESTS
#   #
#   describe 'POST #destroy' do
#     context 'without a user logged in' do
#       it 'should be redirected to sign in' do
#         delete :destroy, id: 1234
#         expect(response).to redirect_to(new_user_session_path)
#       end
#     end
#
#     context 'with a regular user logged in:' do
#       sign_in_user
#
#       it 'should display a non-authorized 401 response' do
#         delete :destroy, id: 1234
#         expect(response.status).to eq(401)
#       end
#     end
#
#     context 'with an admin user logged in:' do
#       sign_in_admin
#
#       it 'should display new affiliate form' do
#         # setup
#         checkin = create(:checkin)
#
#         # exercise
#         delete :destroy, id: checkin.id
#
#         # verify
#         expect(response).to be_success
#         expect(response).to render_template(:new)
#
#         # teardown
#         checkin.user.destroy
#         checkin.destroy
#       end
#     end
#   end
# end
