require 'rspec'
require 'rails_helper'

describe AffiliatesController, type: :controller do
  #
  # CREATE TESTS
  #
  describe 'POST #create' do
    context 'without a user logged in:' do
      it 'should be redirected to sign in' do
        post :create
        expect(response).to redirect_to(new_user_session_path)
      end
    end

    context 'with a regular user logged in:' do
      sign_in_user

      it 'should display a non-authorized 401 response' do
        post :create
        expect(response.status).to eq(401)
      end
    end

    context 'with an admin user logged in with invalid parameters' do
      sign_in_admin

      it 'should not accept parameter and re-render #new' do
        affiliate = build(:invalid)

        post :create, affiliate: {
          name: affiliate.name,
          address1: affiliate.address1,
          address2: affiliate.address2,
          city: affiliate.city,
          state: affiliate.state,
          zipcode: affiliate.zipcode }

        expect(Affiliate.count).to eq(0)
        expect(response).to render_template(:new)
      end
    end

    context 'with an admin user logged in with valid parameters' do
      sign_in_admin

      it 'redirects to created affiliate page' do
        affiliate = build(:affiliate)

        post :create, affiliate: {
          name: affiliate.name,
          address1: affiliate.address1,
          address2: affiliate.address2,
          city: affiliate.city,
          state: affiliate.state,
          zipcode: affiliate.zipcode }

        expect(Affiliate.count).to eq(1)
        expect(response).to redirect_to("/affiliates/#{affiliate.url_value}")

        Affiliate.first.destroy
      end
    end
  end

  #
  # INDEX TESTS
  #
  describe 'GET #index:' do
    it 'assigns @affiliates to the affiliates' do
      affiliate = create(:affiliate)

      get :index, lat: 39.5186002, lon: -104.7613633
      expect(assigns(:affiliates).to_a).to eq([affiliate])

      affiliate.destroy
    end

    it 'has a successful response with empty parameters' do
      get :index
      expect(response).to be_success
      expect(response).to render_template('index')
    end

    it 'has a successful response with lat lon parameters' do
      get :index, lat: 39.5186002, lon: -104.7613633
      expect(response).to be_success
      expect(response).to render_template('index')
    end
  end

  #
  # SHOW TESTS
  #
  describe 'GET #show:' do
    it 'displays the requested affiliate' do
      affiliate = create(:affiliate)

      get :show, id: affiliate.url_value
      expect(response).to be_success
      expect(response).to render_template('show')

      affiliate.destroy
    end
  end

  #
  # NEW TESTS
  #
  describe 'GET #new' do
    context 'without a user logged in' do
      it 'should be redirected to sign in' do
        get :new
        expect(response).to redirect_to(new_user_session_path)
      end
    end

    context 'with a regular user logged in:' do
      sign_in_user

      it 'should display a non-authorized 401 response' do
        get :new
        expect(response.status).to eq(401)
      end
    end

    context 'with an admin user logged in:' do
      sign_in_admin

      it 'should display new affiliate form' do
        get :new
        expect(response).to be_success
        expect(response).to render_template(:new)
      end
    end
  end

  #
  # UPDATE TESTS
  #
  describe 'PUT #update' do
    context 'without a user logged in:' do
      it 'should be redirected to sign in' do
        affiliate = build(:invalid)

        put :update, id: affiliate.url_value, affiliate: {
          name: affiliate.name,
          address1: affiliate.address1,
          address2: affiliate.address2,
          city: affiliate.city,
          state: affiliate.state,
          zipcode: affiliate.zipcode
        }
        expect(response).to redirect_to(new_user_session_path)
      end
    end

    context 'with a regular user logged in:' do
      sign_in_user

      it 'should display a non-authorized 401 response' do
        affiliate = build(:affiliate)

        put :update, id: affiliate.url_value, affiliate: {
          name: affiliate.name,
          address1: affiliate.address1,
          address2: affiliate.address2,
          city: affiliate.city,
          state: affiliate.state,
          zipcode: affiliate.zipcode
        }

        expect(response.status).to eq(401)
      end
    end

    context 'with an admin user logged in with invalid parameters:' do
      sign_in_admin

      it 'should not accept parameter and re-render #edit' do
        affiliate = create(:affiliate)

        put :update, id: affiliate.url_value, affiliate: {
          name: affiliate.name,
          address1: affiliate.address1,
          address2: affiliate.address2,
          city: affiliate.city,
          state: affiliate.state,
          zipcode: 'foobar'
        }

        expect(response).to render_template(:edit)

        affiliate.destroy
      end
    end

    context 'with an admin user logged in with valid parameters:' do
      sign_in_admin

      it 'redirects to updated affiliate page' do
        affiliate = create(:affiliate)

        put :update, id: affiliate.url_value, affiliate: {
          name: affiliate.name,
          address1: affiliate.address1,
          address2: affiliate.address2,
          city: affiliate.city,
          state: affiliate.state,
          zipcode: affiliate.zipcode }

        expect(response).to redirect_to("/affiliates/#{affiliate.url_value}")

        affiliate.destroy
      end
    end
  end

  #
  # DELETE TESTS
  #
  describe 'DELETE #destroy' do
    context 'without a user logged in:' do
      it 'should be redirected to sign in' do
        affiliate = build(:invalid)

        delete :destroy, id: affiliate.url_value
        expect(response).to redirect_to(new_user_session_path)
      end
    end

    context 'with a regular user logged in:' do
      sign_in_user

      it 'should display a non-authorized 401 response' do
        affiliate = build(:invalid)

        delete :destroy, id: affiliate.url_value
        expect(response.status).to eq(401)
      end
    end

    context 'with an admin user logged in:' do
      sign_in_admin

      it 'fails to delete an affiliate that DNE and redirects to affiliates
          page' do
        delete :destroy, id: 'foobar'
        expect(response).to redirect_to('/affiliates')
      end

      it 'successfully deletes an existing affiliate and redirects to affiliates
         page' do
        affiliate = build(:affiliate)

        delete :destroy, id: affiliate.url_value
        expect(response).to redirect_to('/affiliates')
      end
    end
  end

  #
  # CHECKIN TESTS
  #
  describe 'POST #create_checkin' do
    context 'without a user logged in:' do
      it 'should be redirected to sign in' do
        affiliate = build(:invalid)

        post :create_checkin, id: affiliate.url_value
        expect(response).to redirect_to(new_user_session_path)
      end
    end

    context 'with a regular user logged in:' do
      sign_in_user

      it 'creates a checkin and redirects to the affiliate page' do
        affiliate = create(:affiliate)

        post :create_checkin, id: affiliate.url_value
        expect(response).to redirect_to("/affiliates/#{affiliate.url_value}")
        expect(Checkin.count).to eq(1)

        affiliate.destroy
      end
    end

    context 'with an admin user logged in:' do
      sign_in_admin

      it 'creates a checkin and redirects to the affiliate page' do
        affiliate = create(:affiliate)

        post :create_checkin, id: affiliate.url_value
        expect(response).to redirect_to("/affiliates/#{affiliate.url_value}")
        expect(Checkin.count).to eq(2)

        affiliate.destroy
      end
    end
  end

  describe 'GET #index_checkin' do
    context 'without a user logged in:' do
      it 'should be redirected to sign in' do
        affiliate = create(:affiliate)

        get :index_checkins, id: affiliate.url_value
        expect(response).to redirect_to(new_user_session_path)

        affiliate.destroy
      end
    end

    context 'with a regular user logged in:' do
      sign_in_user

      it 'should display a non-authorized 401 response' do
        affiliate = create(:affiliate)

        get :index_checkins, id: affiliate.url_value
        expect(response.status).to eq(401)

        affiliate.destroy
      end
    end

    context 'with an admin user logged in:' do
      sign_in_admin

      it 'assigns @checkins to the affiliates checkins' do
        affiliate = create(:affiliate)
        checkin = create(:checkin)
        checkin.affiliate = affiliate
        checkin.save

        get :index_checkins, id: affiliate.url_value
        expect(assigns(:checkins).to_a).to eq([checkin])

        checkin.destroy
        affiliate.destroy
      end

      it 'renders the index_checkins template' do
        affiliate = create(:affiliate)

        get :index_checkins, id: affiliate.url_value
        expect(response).to be_success
        expect(response).to render_template('index_checkins')

        affiliate.destroy
      end
    end
  end
end
