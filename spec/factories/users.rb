FactoryGirl.define do
  sequence :email do |n|
    "person#{n}@example.com"
  end
end

FactoryGirl.define do
  factory :user, class: 'User' do
    first_name 'Rich'
    last_name 'Froning'
    address1 '123 Address St'
    address2 '#111'
    city 'Knoxville'
    state 'TN'
    zipcode '12345'
    email
    password 'password'
    password_confirmation 'password'
    is_admin false
    association :affiliate, factory: :affiliate, strategy: :build

    factory :user_with_tokens do
      transient do
        token_count 2
      end

      after(:build) do |user, evaluator|
        create_list(:random_token, evaluator.token_count, user: user)
      end
    end
  end
end

FactoryGirl.define do
  factory :admin, parent: :user do
    is_admin true
  end
end
