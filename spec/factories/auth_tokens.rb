FactoryGirl.define do
  sequence :secret_id do |n|
    "MySecret#{n}"
  end

  factory :auth_token do
    secret_id 'MyString'
    hashed_secret 'MyString'
    client_id 'WebApp'
    app_type 'WebApp'
  end

  factory :random_token, class: AuthToken do
    secret_id
    hashed_secret 'MyString'
    client_id 'FactoryClientID'
    app_type 'FactoryAppType'
  end
end
