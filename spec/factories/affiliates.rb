FactoryGirl.define do
  factory :affiliate do
    name 'Test Gym'
    address1 '123 Some Address'
    address2 '#123'
    city 'Denver'
    state 'CO'
    zipcode 80_022
    url_value 'TestGym'

    factory :affiliate_with_reviews do
      transient do
        review_count 2
      end

      after(:build) do |affiliate, evaluator|
        create_list(:review, evaluator.review_count, affiliate: affiliate)
      end
    end
  end

  factory :invalid, class: Affiliate do
    name 'Test Gym'
    address1 '123 Some Address'
    address2 '#123'
    city 'Denver'
    state 'CO'
    url_value 'TestGym'
  end
end
