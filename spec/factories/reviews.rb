FactoryGirl.define do
  sequence(:num_stars, 1) { |n| n }
  sequence(:characteristics, 1) { |c| ["char-#{c}", "char-#{c + 1}"] }

  factory :review, class: Review do
    comment 'This is a comment'
    num_stars
    characteristics
  end
end
