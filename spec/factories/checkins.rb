FactoryGirl.define do
  factory :checkin do
    self.when DateTime.new
    affiliate
    user
  end
end
