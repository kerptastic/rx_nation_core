FactoryGirl.define do
  factory :billing_info do
    card_company 'Visa'
    card_type 'Credit'
    card_number '4234123412341234'
    card_exp_month '08'
    card_exp_year '2018'
  end
end
