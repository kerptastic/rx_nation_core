FactoryGirl.define do
  factory :subscription do
    plan 'Monthly Member'
    cost '5.00'
    joined_date Time.current
    billing_info
    affiliate
  end
end
