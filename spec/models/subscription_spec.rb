require 'rspec'
require 'rails_helper'

describe Subscription, type: :model do
  describe 'Factory Tests' do
    it 'has a valid factory' do
      # setup
      subscription = build(:subscription)

      # exercise
      # handled by FactoryGirl

      # verify
      expect(subscription).to be_valid

      # teardown
      # handled by RSpec
    end
  end

  describe 'Creation Tests' do
    context 'when a Subscription is created:' do
      it 'is invalid without a plan' do
        # setup
        subscription = build(:subscription, plan: nil)

        # exercise
        # handled by FactoryGirl

        # verify
        expect(subscription).to_not be_valid

        # teardown
        # handled by RSpec
      end

      it 'is invalid without a valid plan' do
        # setup
        subscription = build(:subscription, plan: 'foobar')

        # exercise
        # handled by FactoryGirl

        # verify
        expect(subscription).to_not be_valid

        # teardown
        # handled by RSpec
      end

      it 'is invalid without a cost' do
        # setup
        subscription = build(:subscription, cost: nil)

        # exercise
        # handled by FactoryGirl

        # verify
        expect(subscription).to_not be_valid

        # teardown
        # handled by RSpec
      end

      it 'is invalid with a non-numeric cost' do
        # setup
        subscription = build(:subscription, cost: 'foobar')

        # exercise
        # handled by FactoryGirl

        # verify
        expect(subscription).to_not be_valid

        # teardown
        # handled by RSpec
      end

      it 'is invalid with more than 2 decimal places in the cost' do
        # setup
        subscription = build(:subscription, cost: 1.123)

        # exercise
        # handled by FactoryGirl

        # verify
        expect(subscription).to_not be_valid

        # teardown
        # handled by RSpec
      end

      it 'is invalid with more than 5 dollar digits the cost' do
        # setup
        subscription = build(:subscription, cost: 123_456.00)

        # exercise
        # handled by FactoryGirl

        # verify
        expect(subscription).to_not be_valid

        # teardown
        # handled by RSpec
      end

      it 'is invalid without a joined date' do
        # setup
        subscription = build(:subscription, joined_date: nil)

        # exercise
        # handled by FactoryGirl

        # verify
        expect(subscription).to_not be_valid

        # teardown
        # handled by RSpec
      end

      it 'is invalid without billing info' do
        # setup
        subscription = build(:subscription, billing_info: nil)

        # exercise
        # handled by FactoryGirl

        # verify
        expect(subscription).to_not be_valid

        # teardown
        # handled by RSpec
      end

      it 'is invalid without an affiliate' do
        # setup
        subscription = build(:subscription, affiliate: nil)

        # exercise
        # handled by FactoryGirl

        # verify
        expect(subscription).to_not be_valid

        # teardown
        # handled by RSpec
      end
    end
  end
end
