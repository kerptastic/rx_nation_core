require 'rails_helper'

describe AuthToken, type: :model do
  #
  # Factory tests
  #
  describe 'Factory Tests' do
    it 'has a valid factory' do
      # setup
      auth_token = build(:auth_token)

      # exercise
      # handled by FactoryGirl

      # verify
      expect(auth_token).to be_valid

      # teardown
      # handled by RSpec
    end
  end

  #
  # Model tests
  #
  describe 'Model Creation Tests' do
    context 'when an auth token is created' do
      it 'is invalid without a secret_id' do
        # setup
        auth_token = build(:auth_token, secret_id: nil)

        # exercise
        # handled by FactoryGirl

        # verify
        expect(auth_token).to_not be_valid

        # teardown
        # handled by RSpec
      end

      it 'is invalid without a hashed_secret' do
        # setup
        auth_token = build(:auth_token, hashed_secret: nil)

        # exercise
        # handled by FactoryGirl

        # verify
        expect(auth_token).to_not be_valid

        # teardown
        # handled by RSpec
      end
    end

    describe '#default_values' do
      it 'sets a default expiration time upon creation' do
        # setup
        auth_token = build(:auth_token)

        # exercise
        # handled by FactoryGirl

        # verify
        expect(auth_token.expiration).not_to be nil

        # teardown
        # handled by RSpec
      end
    end

    describe '#secret?' do
      it 'returns false if the hashed_secret is not equal to whats passed in' do
        # setup
        auth_token = build(:auth_token)
        allow(auth_token).to receive(:password_from_hash)
          .and_return 'foobar'

        # exercise
        # handled by FactoryGirl

        # verify
        expect(auth_token.secret?('not_foobar')).to be_falsey

        # teardown
        # handled by RSpec
      end

      it 'returns true if the hashed_secret is equal to whats passed in' do
        # setup
        auth_token = build(:auth_token)
        allow(auth_token).to receive(:password_from_hash)
          .and_return 'foobar'

        # exercise
        # handled by FactoryGirl

        # verify
        expect(auth_token.secret?('foobar')).to be_truthy

        # teardown
        # handled by RSpec
      end
    end

    describe '.find_authenticated' do
      it 'returns a valid token when the credentials match an existing token' do
        # setup
        credentials = { secret_id: 'MyString', client_id: 'WebApp',
                        secret: 'MyString' }
        auth_token = create(:auth_token)
        allow_any_instance_of(AuthToken).to receive(:password_from_hash)
          .and_return 'MyString'

        # exercise
        token = AuthToken.find_authenticated(credentials)

        # verify
        expect(token).to_not be_nil

        # teardown
        auth_token.destroy
      end

      context 'returns a nil when the credentials dont match existing token' do
        it 'returns nil when secret_id isnt found' do
          # setup
          auth_token = create(:auth_token)
          allow_any_instance_of(AuthToken).to receive(:password_from_hash)
            .and_return 'MyString'

          credentials = {
            secret_id: '',
            client_id: 'WebApp',
            secret: 'MyString' }

          # exercise
          token = AuthToken.find_authenticated(credentials)

          # verify
          expect(token).to be_nil

          # teardown
          auth_token.destroy
        end

        it 'returns nil when client_id isnt found' do
          # setup
          auth_token = create(:auth_token)
          allow_any_instance_of(AuthToken).to receive(:password_from_hash)
            .and_return 'MyString'

          credentials = { secret_id: 'MyString',
                          client_id: '',
                          secret: 'MyString' }

          # exercise
          token = AuthToken.find_authenticated(credentials)

          # verify
          expect(token).to be_nil

          # teardown
          auth_token.destroy
        end

        it 'returns nil when secret isnt found' do
          # setup
          auth_token = create(:auth_token)
          allow_any_instance_of(AuthToken).to receive(:password_from_hash)
            .and_return 'MyString'

          credentials = { secret_id: 'MyString', client_id: 'WebApp',
                          secret: '' }

          # exercise
          token = AuthToken.find_authenticated(credentials)

          # verify
          expect(token).to be_nil

          # teardown
          auth_token.destroy
        end
      end
    end
  end
end
