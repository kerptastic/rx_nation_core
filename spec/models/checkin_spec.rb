require 'rspec'
require 'rails_helper'

describe Checkin, type: :model do
  describe 'Factory Tests:' do
    it 'has a valid factory' do
      # setup
      checkin = build(:checkin)

      # exercise
      # handled by FactoryGirl

      # verify
      expect(checkin).to be_valid

      # teardown
      checkin.user.destroy
      checkin.destroy
    end
  end

  describe 'creation: ' do
    context 'when a Checkin is created' do
      it 'is invalid without a date when the checkin occured' do
        # setup
        checkin = build(:checkin, when: nil)

        # exercise
        # handled by FactoryGirl

        # verify
        expect(checkin).to_not be_valid

        # teardown
        checkin.user.destroy
        checkin.destroy
      end

      it 'is invalid without an affiliate' do
        # setup
        checkin = build(:checkin, affiliate: nil)

        # exercise
        # handled by FactoryGirl

        # verify
        expect(checkin).to_not be_valid

        # teardown
        checkin.user.destroy
        checkin.destroy
      end

      it 'is invalid without a user' do
        # setup
        checkin = build(:checkin, user: nil)

        # exercise
        # handled by FactoryGirl

        # verify
        expect(checkin).to_not be_valid

        # teardown
        checkin.destroy
      end
    end
  end
end
