require 'rspec'
require 'rails_helper'

describe Review, type: :model do
  #
  # Factory tests
  #
  describe 'Factory Tests' do
    it 'has a valid factory' do
      # setup
      review = build(:review)

      # exercise
      # handled by FactoryGirl

      # verify
      expect(review).to be_valid

      # teardown
      # handled by RSpec
    end
  end

  describe 'Model Creation Tests' do
    context 'when a Review is created' do
      it 'is invalid when a number of stars is not provided' do
        # setup
        review = build(:review, num_stars: nil)

        # exercise
        # handled by FactoryGirl

        # verify
        expect(review).to_not be_valid

        # teardown
        # handled by RSpec
      end

      it 'is invalid when a negative number of stars are provided' do
        # setup
        review = build(:review, num_stars: -1)

        # exercise
        # handled by FactoryGirl

        # verify
        expect(review).to_not be_valid

        # teardown
        # handled by RSpec
      end

      it 'is invalid when more than 5 stars are provided' do
        # setup
        review = build(:review, num_stars: 6)

        # exercise
        # handled by FactoryGirl

        # verify
        expect(review).to_not be_valid

        # teardown
        # handled by RSpec
      end

      it 'is invalid when a comment is more than 500 characters' do
        # setup
        review = build(:review, comment: 'a' * 501)

        # exercise
        # handled by FactoryGirl

        # verify
        expect(review).to_not be_valid

        # teardown
        # handled by RSpec
      end
    end
  end
end
