require 'rspec'
require 'rails_helper'

describe Affiliate, type: :model do
  #
  # Factory tests
  #
  describe 'Factory Tests' do
    it 'has a valid factory' do
      # setup
      affiliate = build(:affiliate)

      # exercise
      # handled by FactoryGirl

      # verify
      expect(affiliate).to be_valid

      # teardown
      # handled by RSpec
    end
  end

  #
  # Model tests
  #
  describe 'Model Creation Tests' do
    context 'when an affiliate is created' do
      it 'is invalid without a name' do
        # setup
        affiliate = build(:affiliate, name: nil)

        # exercise
        # handled by FactoryGirl

        # verify
        expect(affiliate).to_not be_valid

        # teardown
        # handled by RSpec
      end

      it 'is invalid without an address1' do
        # setup
        affiliate = build(:affiliate, address1: nil)

        # exercise
        # handled by FactoryGirl

        # verify
        expect(affiliate).to_not be_valid

        # teardown
        # handled by RSpec
      end

      it 'is invalid without an city' do
        # setup
        affiliate = build(:affiliate, city: nil)

        # exercise
        # handled by FactoryGirl

        # verify
        expect(affiliate).to_not be_valid

        # teardown
        # handled by RSpec
      end

      it 'is invalid without an state' do
        # setup
        affiliate = build(:affiliate, state: nil)

        # exercise
        # handled by FactoryGirl

        # verify
        expect(affiliate).to_not be_valid

        # teardown
        # handled by RSpec
      end

      it 'is invalid without a zipcode' do
        # setup
        affiliate = build(:affiliate, zipcode: nil)

        # exercise
        # handled by FactoryGirl

        # verify
        expect(affiliate).to_not be_valid

        # teardown
        # handled by RSpec
      end
    end
  end

  describe '#avg_review' do
    it 'returns an average review when there are reviews' do
      # setup
      affiliate = build(:affiliate_with_reviews)

      # exercise
      # handled by FactoryGirl

      # verify
      expect(affiliate.reviews.length).to eq 2
      expect(affiliate.avg_review).to eq 1.5

      # teardown
      # handled by RSpec
    end

    it 'returns a zero average review when there are no reviews' do
      # setup
      affiliate = build(:affiliate)

      # exercise
      # hanlded by FactoryGirl

      # verify
      expect(affiliate.reviews.length).to eq 0
      expect(affiliate.avg_review).to eq 0.0

      # teardown
      # handled by RSpec
    end
  end

  describe '#characteristics' do
    it 'returns list of all characteristics when there are reviews' do
      # setup
      affiliate = build(:affiliate_with_reviews)

      # exercise
      # handled by FactoryGirl

      # verify
      expect(affiliate.reviews.length).to eq 2
      expect(affiliate.characteristics).to eq 'char-3, char-4, char-5'

      # teardown
      # handled by RSpec
    end

    it "returns nothing when there aren't any reviews" do
      # setup
      affiliate = build(:affiliate)

      # exercise
      # hanlded by FactoryGirl

      # verify
      expect(affiliate.reviews.length).to eq 0
      expect(affiliate.characteristics).to eq ''

      # teardown
      # handled by RSpec
    end
  end

  describe '#address' do
    it 'returns a printed full address, with address2 when defined' do
      # setup
      affiliate = build(:affiliate)

      # exercise
      # hanlded by FactoryGirl

      # verify
      expect(affiliate.address).to eq '123 Some Address #123, Denver, CO 80022'

      # teardown
      # handled by RSpec
    end

    it 'returns a printed full address, without address2 when not defined' do
      # setup
      affiliate = build(:affiliate, address2: nil)

      # exercise
      # handled by FactoryGirl

      # verify
      expect(affiliate.address).to eq '123 Some Address, Denver, CO 80022'

      # teardown
      # handled by RSpec
    end
  end

  describe '#set_distance_from_search' do
    it 'returns 0 when the coordinates are the same' do
      # setup
      affiliate = build(:affiliate)
      affiliate.geocode

      # exercise
      # hanlded by FactoryGirl

      # verify
      expect(affiliate.set_distance_from_search([39.725449, -105.012704])).to eq 0

      # teardown
      # handled by RSpec
    end

    it 'returns a distance in mi. for the distance of two known points' do
      # setup
      affiliate = build(:affiliate)
      affiliate.geocode
      # affiliate.coordinates = [, ] # some point in denver

      # exercise
      # hanlded by FactoryGirl

      # verify against another point in denver thats calculated online
      expect(affiliate.set_distance_from_search([39.739971,
                                             -104.919320])).to eq 5.06

      # teardown
      # handled by RSpec
    end
  end

  describe '#make_url_value' do
    it 'returns a camel case, space removed, gym name' do
      expect(build(:affiliate).url_value).to eq('TestGym')
    end
  end
end
