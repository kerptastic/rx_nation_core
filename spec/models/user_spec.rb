require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'Factory Tests:' do
    it 'has a valid factory' do
      # setup
      user = build(:user)

      # exercise
      # handled by FactoryGirl

      # verify
      expect(user).to be_valid

      # teardown
      # handled by RSpec
    end
  end

  describe 'creation:' do
    context 'when a User is created' do
      it 'is invalid without a first name' do
        # setup
        user = build(:user, first_name: nil)

        # exercise
        # handled by FactoryGirl

        # verify
        expect(user).to_not be_valid

        # teardown
        # handled by RSpec
      end

      it 'is invalid without a last name' do
        # setup
        user = build(:user, last_name: nil)

        # exercise
        # handled by FactoryGirl

        # verify
        expect(user).to_not be_valid

        # teardown
        # handled by RSpec
      end

      it 'is invalid without an address1' do
        # setup
        user = build(:user, address1: nil)

        # exercise
        # handled by FactoryGirl

        # verify
        expect(user).to_not be_valid

        # teardown
        # handled by RSpec
      end

      it 'is invalid without a city' do
        # setup
        user = build(:user, city: nil)

        # exercise
        # handled by FactoryGirl

        # verify
        expect(user).to_not be_valid

        # teardown
        # handled by RSpec
      end

      it 'is invalid without a state' do
        # setup
        user = build(:user, state: nil)

        # exercise
        # handled by FactoryGirl

        # verify
        expect(user).to_not be_valid

        # teardown
        # handled by RSpec
      end

      it 'is invalid without a zipcode' do
        # setup
        user = build(:user, zipcode: nil)

        # exercise
        # handled by FactoryGirl

        # verify
        expect(user).to_not be_valid

        # teardown
        # handled by RSpec
      end

      it 'is invalid without an affiliate gym' do
        # setup
        user = build(:user, affiliate: nil)

        # exercise
        # handled by FactoryGirl

        # verify
        expect(user).to_not be_valid

        # teardown
        # handled by RSpec
      end

      it 'is invalid with a first_name shorter than 1 character' do
        # setup
        user = build(:user, first_name: '')

        # exercise
        # handled by FactoryGirl

        # verify
        expect(user).to_not be_valid

        # teardown
        # handled by RSpec
      end

      it 'is invalid with a first_name more than 30 characters' do
        # setup
        user = build(:user, first_name: 'a' * 31)

        # exercise
        # handled by FactoryGirl

        # verify
        expect(user).to_not be_valid

        # teardown
        # handled by RSpec
      end

      it 'is invalid with a last_name shorter than 1 character' do
        # setup
        user = build(:user, last_name: '')

        # exercise
        # handled by FactoryGirl

        # verify
        expect(user).to_not be_valid

        # teardown
        # handled by RSpec
      end

      it 'is invalid with a last_name more than 40 characters' do
        # setup
        user = build(:user, last_name: 'a' * 41)

        # exercise
        # handled by FactoryGirl

        # verify
        expect(user).to_not be_valid

        # teardown
        # handled by RSpec
      end

      it 'is invalid with a address1 shorter than 1 character' do
        # setup
        user = build(:user, address1: '')

        # exercise
        # handled by FactoryGirl

        # verify
        expect(user).to_not be_valid

        # teardown
        # handled by RSpec
      end

      it 'is invalid with a address1 more than 50 characters' do
        # setup
        user = build(:user, address1: 'a' * 51)

        # exercise
        # handled by FactoryGirl

        # verify
        expect(user).to_not be_valid

        # teardown
        # handled by RSpec
      end

      it 'is invalid with a city shorter than 1 character' do
        # setup
        user = build(:user, city: '')

        # exercise
        # handled by FactoryGirl

        # verify
        expect(user).to_not be_valid

        # teardown
        # handled by RSpec
      end

      it 'is invalid with a city more than 50 characters' do
        # setup
        user = build(:user, city: 'a' * 51)

        # exercise
        # handled by FactoryGirl

        # verify
        expect(user).to_not be_valid

        # teardown
        # handled by RSpec
      end

      it 'is invalid with a state that is not 2 characters' do
        # setup
        user1 = build(:user, state: 'aaa')
        user2 = build(:user, state: 'aaa')
        user3 = build(:user, state: 'aa')

        # exercise
        # handled by FactoryGirl

        # verify
        expect(user1).to_not be_valid
        expect(user2).to_not be_valid
        expect(user3).to be_valid

        # teardown
        # handled by RSpec
      end

      it 'is invalid with a zipcode that is not 5 characters' do
        # setup
        user1 = build(:user, zipcode: '1')
        user2 = build(:user, zipcode: '12')
        user3 = build(:user, zipcode: '123')
        user4 = build(:user, zipcode: '1234')
        user5 = build(:user, zipcode: '12345')
        user6 = build(:user, zipcode: '123456')

        # exercise
        # handled by FactoryGirl

        # verify
        expect(user1).to_not be_valid
        expect(user2).to_not be_valid
        expect(user3).to_not be_valid
        expect(user4).to_not be_valid
        expect(user5).to be_valid
        expect(user6).to_not be_valid

        # teardown
        # handled by RSpec
      end
    end
  end
end
