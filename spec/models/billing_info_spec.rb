require 'rspec'
require 'rails_helper'

describe BillingInfo, type: :model do
  #
  # Factory tests
  #
  describe 'Factory Tests' do
    it 'has a valid factory' do
      # setup
      billing_info = build(:billing_info)

      # exercise
      # handled by FactoryGirl

      # verify
      expect(billing_info).to be_valid

      # teardown
      # handled by RSpec
    end
  end

  #
  # Model tests
  #
  describe 'Model Creation Tests' do
    it 'is invalid without a card company' do
      # setup
      billing_info = build(:billing_info, card_company: nil)

      # exercise
      # handled by FactoryGirl

      # verify
      expect(billing_info).to_not be_valid

      # teardown
      # handled by RSpec
    end

    it 'is invalid with an unknown card company' do
      # setup
      billing_info = build(:billing_info, card_company: 'foobar')

      # exercise
      # handled by FactoryGirl

      # verify
      expect(billing_info).to_not be_valid

      # teardown
      # handled by RSpec
    end

    it 'is invalid without a card type' do
      # setup
      billing_info = build(:billing_info, card_type: nil)

      # exercise
      # handled by FactoryGirl

      # verify
      expect(billing_info).to_not be_valid

      # teardown
      # handled by RSpec
    end

    it 'is invalid with an unknown card type' do
      # setup
      billing_info = build(:billing_info, card_type: 'foobar')

      # exercise
      # handled by FactoryGirl

      # verify
      expect(billing_info).to_not be_valid

      # teardown
      # handled by RSpec
    end

    it 'is invalid without a card number' do
      # setup
      billing_info = build(:billing_info, card_number: nil)

      # exercise
      # handled by FactoryGirl

      # verify
      expect(billing_info).to_not be_valid

      # teardown
      # handled by RSpec
    end

    it 'is invalid without a card expiration month' do
      # setup
      billing_info = build(:billing_info, card_exp_month: nil)

      # exercise
      # handled by FactoryGirl

      # verify
      expect(billing_info).to_not be_valid

      # teardown
      # handled by RSpec
    end

    it 'is invalid without a card expiration year' do
      # setup
      billing_info = build(:billing_info, card_exp_year: nil)

      # exercise
      # handled by FactoryGirl

      # verify
      expect(billing_info).to_not be_valid

      # teardown
      # handled by RSpec
    end

    context 'With invalid credit card numbers (credit_card_validator)' do
      it 'is invalid with a bad visa credit card number' do
        # setup
        billing_info = build(:billing_info, card_company: 'Visa',
                                            card_number: '12')

        # exercise
        # handled by FactoryGirl

        # verify
        expect(billing_info).to_not be_valid

        # teardown
        # handled by RSpec
      end

      it 'is invalid with a bad mastercard credit card number' do
        # setup
        billing_info = build(:billing_info, card_company: 'Mastercard',
                                            card_number: '12')

        # exercise
        # handled by FactoryGirl

        # verify
        expect(billing_info).to_not be_valid

        # teardown
        # handled by RSpec
      end

      it 'is invalid with a bad discover credit card number' do
        # setup
        billing_info = build(:billing_info, card_company: 'Discover',
                                            card_number: '12')

        # exercise
        # handled by FactoryGirl

        # verify
        expect(billing_info).to_not be_valid

        # teardown
        # handled by RSpec
      end

      it 'is invalid with a bad american express credit card number' do
        # setup
        billing_info = build(:billing_info, card_company: 'American Express',
                                            card_number: '12')

        # exercise
        # handled by FactoryGirl

        # verify
        expect(billing_info).to_not be_valid

        # teardown
        # handled by RSpec
      end
    end
  end
end
