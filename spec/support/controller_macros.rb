#
# Macros to be used by all Controllers
#
module ControllerMacros
  def sign_in_user
    before(:each) do
      @user ||= FactoryGirl.create :user
      sign_in @user
      # ControllerMacros.mock_sign_in @user
    end

    after(:each) do
      @user.destroy
    end
  end

  def sign_in_admin
    before(:each) do
      @admin ||= FactoryGirl.create :admin
      sign_in @admin
      # ControllerMacros.mock_sign_in @admin
    end

    after(:each) do
      @admin.destroy
    end
  end

  private

  # def self.mock_sign_in(user)
  #   if user.nil?
  #     allow(@request.env['warden']).to receive(:authenticate!).and_throw(
  #       :warden, {:scope => :user})
  #     allow(controller).to receive(:current_user).and_return(nil)
  #   else
  #     allow(@request.env['warden']).to receive(:authenticate!).and_return(
  #       user)
  #     allow(controller).to receive(:current_user).and_return(user)
  #   end
  # end
end
