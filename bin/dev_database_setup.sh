rake db:drop RAILS_ENV=development;
rake db:seed RAILS_ENV=development;
rake db:mongoid:create_indexes RAILS_ENV=development;
