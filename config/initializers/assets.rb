# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css.scss, and all non-JS/CSS in app/assets folder
# are already added.
# Rails.application.config.assets.precompile += %w( search.js )
Rails.application.config.assets.precompile +=
  %w(search_results.css
     search_results.js
     star-rating.min.css
     star-rating.min.js
     affiliates.css
     slick.css
     slick.min.js
     slick-theme.css
     affiliates.js
  )
